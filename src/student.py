"""Student module
get_logins parses data.txt with fio and logins, example
Иванов Иван Иванович;login
"""
import requests


def get_logins(filename, url="https://gitlab.com/"):
    """Get fio and logins from file filename
    
    Args:
        filename - path to the file witch fio and logins. Example:
            Иванов Иван Иванович;login

    Returns:
        list of dicts with "fio" and "logins" parsed from filename
        Example:
            [
                {"fio": "Иванов Иван Иванович, "login", "login"},
                {"fio": "Петров Петр Петрович, "login", "pyotr"}
            ]

    Raises:
        ValueError: if bad filename format (no ; in a line)
        RuntimeError: if login is not registred at gitlab.com

    """

    res = []
    with open(filename, "r", encoding="utf-8") as file_:
        for line in file_:
            print(line)
            try:
                fio, login = line.split(";")
                fio = fio.strip()
                login = login.strip()

                print(f"{fio=} {login=}")
                res.append({
                    "fio": fio,
                    "login": login,
                })

                repo_url = f"{url}{login}"
                print(f"repo is {repo_url}")
                response = requests.get(repo_url)
                print(response)
                if response.status_code != 200:
                    raise RuntimeError("No such user is registred on gitlab!")
            except ValueError as err:
                print(f"Cannot parse file: {err}")
                raise
    return res


def main():
    """Main function
    """
    filename = "data/data.txt"
    return get_logins(filename)

if __name__ == "__main__":
    print(main())
